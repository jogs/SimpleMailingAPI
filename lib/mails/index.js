/*
** Dependencias
*/
var module_mail = require('express')();
var email   = require("./../../node_modules/emailjs/email");
var mailServer  = email.server.connect({
  user:    "YOUR_MAIL", 
  password:"YOUR_PASS", 
  host:    "smtp.YOUR_HOST.com", 
  ssl:     true
});


module_mail.route('/mail')
	.all(function(req, res, next){
		console.log(req.method, req.path, req.body);
		res.set('Content-Type', 'application/json');
		next();
	})
	.post(function(req, res){
		var newMail = req.body.mail;
		mailServer.send(newMail, function(err, message) {
			//console.log(err || message)

			if(err){
				newMail.success = false;
				res
					.status(200)
					.send({
						"mail": newMail
					});
				console.log("--> Mail is not sended, pleace configure lib/mails/index.js <--");
			}
			else{
				newMail.success = true;
				res
					.status(201)
					.send({
						"mail": newMail
					});
				console.log("Mail is sended");
			}
		});
	});

module.exports = module_mail;