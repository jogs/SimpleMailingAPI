var request = require('supertest-as-promised');
var api = require('../index.js');
var host = process.env.API_TEST_HOST || api;
request = request(host);

describe('Resource /mail', function(){
	
	var data = {
		"mail": {
			"from": "noreply@skypunch.com.mx",//insert your no reply mail
			"to": "joaquin.garcia@skypunch.com.mx",//insert your mail
			"subject": "this is a test",
			"text": "this is a test, should be success <br> Pleace NO REPLY"
		}
	}
	
	describe('POST (CREATE)', function(){
		it('Deberia enviar un mail', function(done) {
			request
				.post('/mail')
				.set('Accept', 'application/json')
				.send(data)
				.expect(201)
				.expect('Content-Type', /application\/json/)
				.end(function(err, res) {
					var body = res.body;
					expect(body).to.have.property('mail');
					var mail = body.mail;
					//console.log('POST ', mail);
					expect(mail).to.have.property("from", "noreply@skypunch.com.mx");
					expect(mail).to.have.property("to", "joaquin.garcia@skypunch.com.mx");
					expect(mail).to.have.property("subject", "this is a test");
					expect(mail).to.have.property("text", "this is a test, should be success <br> Pleace NO REPLY");
					expect(mail).to.have.property('success', true);
					done();
				});
		});
	});
});