# Simple Mailing API

This API provide a mailing service



## Install
    git clone git@gitlab.com:jogs/SimpleMailingAPI.git
    npm install

## Configure
### Change the Test file

    // test/mails.js :: line 11
    "to": "joaquin.garcia@skypunch.com.mx", // change YOUR_MAIL
### Change the mail service

    // lib/mails/index.js :: line 6-10
    var mailServer  = email.server.connect({
      user:    "no.reply@skypunch.com.mx", //change YOUR_MAIL 
      password:"YOUR_PASS", //change YOUR_PASS 
      host:    "smtp.YOUR_HOST.com", // change YOUR_HOST
      ssl:     true
    });
    
[___See the EmailJS project___](https://github.com/eleith/emailjs)
## Test
    make test