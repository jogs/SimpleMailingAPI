var server = require('express')();
var bodyParser = require('body-parser');
var port = process.env.PORT || 1337;

server.use(bodyParser.json('application/json'));

var mails = require('./lib/mails');
server.use(mails);

server.listen(port, function() {
	console.log('Servidor ejecutando en http://localhost:' + port)
});